#pragma once

#include <wheels/support/memspan.hpp>

namespace context {

using StackView = wheels::MemSpan;

}  // namespace context
