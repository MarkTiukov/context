#pragma once

#include <context/stack_view.hpp>

namespace context {

StackView ThisThreadStack();

}  // namespace context
