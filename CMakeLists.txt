cmake_minimum_required(VERSION 3.9)
project(context)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_compile_options(-Wall -Wextra -Wpedantic -g -fno-omit-frame-pointer)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "bin")

include(cmake/Sanitize.cmake)

add_subdirectory(third_party)

option(CONTEXT_DEVELOPER "Context development mode" OFF)

include(cmake/Platform.cmake)
add_subdirectory(context)

if(CONTEXT_DEVELOPER)
    add_subdirectory(tests)
endif()
